﻿using System.Threading.Tasks;
using Forum.BLL.Interfaces;
using Forum.WebApi.Controllers;
using Moq;
using NUnit.Framework;

namespace Forum.Tests.ControllerTests
{
    public class PostsControllerTests
    {
        private Mock<IPostService> moq;
        private PostsController controller;

        [SetUp]
        public void SetUp()
        {
            moq = new Mock<IPostService>();
            controller = new PostsController(moq.Object);
        }

        [Test]
        public async Task PostsController_GetPosts_ServiceInvoke()
        {
            await controller.GetPosts();
            
            moq.Verify(s=> s.GetPosts() , Times.Once);
        }
        
        [Test]
        public void PostsController_ShowPosts_ServiceInvoke()
        {
            controller.ShowPosts(It.IsAny<int>(), It.IsAny<int>());
            
            moq.Verify(s=> s.GetPostsAtPage(It.IsAny<int>() , It.IsAny<int>()) , Times.Once);
        }
        
        [Test]
        public async Task PostsController_GetPost_ServiceInvoke() 
        {
            await controller.GetPost(It.IsAny<int>());

            moq.Verify(service => service.GetPost(It.IsAny<int>()), Times.Once);
        }
        
        [Test]
        public async Task PostsController_DeletePost_ServiceInvoke()
        {
            await controller.DeletePost(It.IsAny<int>());

            moq.Verify(service => service.DeletePost(It.IsAny<int>()), Times.Once);
        }

        [Test]
        public void PostsController_PostsCount_ServiceInvoke()
        {
            controller.PostsCount();
            
            moq.Verify(s => s.PostsCount() , Times.Once);
        }

        [Test]
        public async Task PostsControllers_GetPostComments_ServiceInvoke()
        {
            await controller.GetPostComments(It.IsAny<int>());
            
            moq.Verify(s => s.GetPostComments(It.IsAny<int>()));
        }
        
        [Test]
        public async Task PostsControllers_DeleteComment_ServiceInvoke()
        {
            await controller.DeleteComment(It.IsAny<int>());
            
            moq.Verify(s => s.DeleteComment(It.IsAny<int>()));
        }
        
        [Test]
        public async Task PostsControllers_PostsCommentsCount_ServiceInvoke()
        {
            await controller.PostsCommentsCount(It.IsAny<int>());
            
            moq.Verify(s => s.GetPostComments(It.IsAny<int>()));
        }
    }
}