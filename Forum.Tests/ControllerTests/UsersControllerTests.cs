﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.WebApi.Controllers;
using Forum.WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace Forum.Tests.ControllerTests
{
    public class UsersControllerTests
    {
        private Mock<IUserService> moq;
        private UsersController controller;


        [SetUp]
        public void SetUp()
        {
            moq = new Mock<IUserService>();
            controller = new UsersController(moq.Object);
        }

        [Test]
        public async Task UsersController_Register_ServiceInvoke()
        {
            await controller.Register(new SignUpView{Password = "pass" , RetryPassword = "pass" , Username = "test"});
            
            moq.Verify(s => s.CreateUser(It.IsAny<UserDTO>() , It.IsAny<string>() ) , Times.Once);
        }

        [Test]
        public async Task UsersController_Login_ServiceInvoke()
        {
            await controller.Login(new LoginView { Password = "pass", UserName = "user" });
            
            moq.Verify(s => s.Login(It.IsAny<string>() , It.IsAny<string>() ), Times.Once);
        }

        [Test]
        public async Task UsersController_GetAllUsers_ServiceInvoke()
        {
            await controller.GetAllUsers();
             
             moq.Verify(s => s.GetAllUsers() , Times.Once);
        }

    }
}