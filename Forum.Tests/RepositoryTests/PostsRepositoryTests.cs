using System;
using System.Linq;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Repositories;
using Forum.Tests.UnitTestHelpers;
using NUnit.Framework;

namespace Forum.Tests.RepositoryTests
{
    public class PostsRepositoryTests
    {
        [Test]
        public async Task PostsRepository_GetAll_ReturnAllValues()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var postsRepository = new PostsRepository(context);
                var posts = await postsRepository.GetAll();

                Assert.AreEqual(3, posts.Count());
            }
        }

        [Test]
        public async Task PostsRepository_GetById_ReturnSingleValue()
        {
            using (ForumContext context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var postsRepository = new PostsRepository(context);
                var post = await postsRepository.GetById(3);
                
                Assert.AreEqual("Third post" , post.Text);
                Assert.AreEqual("Post 3" , post.Title);
                Assert.AreEqual(new DateTime(2021,7,21) , post.Published);
            }
        }

        [Test]
        public async Task PostsRepository_CreatePost_AddsToInMemoryDatabase()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var postsRepository = new PostsRepository(context);
                await postsRepository.Create(new Post {PostId = 4 , Text = "Fourth post" , Title = "Post 4" , Published = new DateTime(2021, 1, 1) , UserId = 2});
                context.SaveChanges();
                
                Assert.AreEqual(4 , context.Posts.Count());
            }
        }

        [Test]
        public async Task PostsRepository_DeleteById_DeletesFromDatabase()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var postsRepository = new PostsRepository(context);
                postsRepository.Delete(2);
                context.SaveChanges();
                var allPosts = await postsRepository.GetAll();
                
                Assert.AreEqual(2 , allPosts.Count());
            }
        }

        [Test]
        public async Task PostsRepository_UpdatePost_UpdatesPostInDatabase()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var postsRepository = new PostsRepository(context);
                postsRepository.Update(new Post {PostId = 3 , Text = "Third post updated" , Title = "Post 3 updated" , Published = new DateTime(2021, 7, 21) , UserId = 2});
                context.SaveChanges();
                var updatedPost = await postsRepository.GetById(3);
                
                Assert.AreEqual("Third post updated" , updatedPost.Text);
                Assert.AreEqual("Post 3 updated" , updatedPost.Title);
            }
        }

        [Test]
        public void PostsRepository_FindPost_ReturnsPostsByCondition()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var postsRepository = new PostsRepository(context);
                var foundPosts = postsRepository.Find(p => p.PostId == 1).FirstOrDefault();
                
                Assert.AreEqual("First post" , foundPosts.Text);
                Assert.AreEqual("Post 1" , foundPosts.Title);
            }
        }
    }
}