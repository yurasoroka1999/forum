﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Repositories;
using Forum.Tests.UnitTestHelpers;
using NUnit.Framework;

namespace Forum.Tests.RepositoryTests
{
    public class CommentsRepositoryTests
    {
        [Test]
        public async Task CommentsRepository_GetAll_ReturnAllValues()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var commentsRepository = new CommentsRepository(context);
                var comments = await commentsRepository.GetAll();

                Assert.AreEqual(3, comments.Count());
            }
        }

        [Test]
        public async Task CommentsRepository_GetById_ReturnSingleValue()
        {
            using (ForumContext context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var commentsRepository = new CommentsRepository(context);
                var comment = await commentsRepository.GetById(1);
                
                Assert.AreEqual("First comment" , comment.Message);
                Assert.AreEqual(1 , comment.UserId);
                Assert.AreEqual(new DateTime(2020 , 12,12) , comment.CreationDate);
            }
        }

        [Test]
        public async Task CommentsRepository_CreateComment_AddsToInMemoryDatabase()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var commentsRepository = new CommentsRepository(context);
                await commentsRepository.Create(new Comment { PostId = 1, Message = "Fourth comment", CommentId = 4 , CreationDate = new DateTime(2019 , 12,12), UserId = 2});
                context.SaveChanges();
                
                Assert.AreEqual(4 , context.Comments.Count());
            }
        }

        [Test]
        public async Task CommentsRepository_DeleteById_DeletesFromDatabase()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var commentsRepository = new CommentsRepository(context);
                commentsRepository.Delete(2);
                context.SaveChanges();
                var allComments = await commentsRepository.GetAll();
                
                Assert.AreEqual(2 , allComments.Count());
            }
        }

        [Test]
        public async Task CommentsRepository_UpdateComment_UpdatesCommentInDatabase()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var commentsRepository = new CommentsRepository(context);
                commentsRepository.Update(new Comment { PostId = 3, Message = "Third comment updated", CommentId = 3 , CreationDate = new DateTime(2010 , 10,10), UserId = 2});
                context.SaveChanges();
                var updatedComment = await commentsRepository.GetById(3);
                
                Assert.AreEqual("Third comment updated" , updatedComment.Message);
                Assert.AreEqual(new DateTime(2010,10,10) , updatedComment.CreationDate);
            }
        }

        [Test]
        public void CommentsRepository_FindCommets_ReturnsCommentByCondition()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var commentsRepository = new CommentsRepository(context);
                var foundComment = commentsRepository.Find(p => p.CommentId == 2).FirstOrDefault();
                
                Assert.AreEqual("Second comment" , foundComment.Message);
                Assert.AreEqual(2 , foundComment.UserId);
            }
        }
    }
}