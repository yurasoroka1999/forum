﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Repositories;
using Forum.Tests.UnitTestHelpers;
using NUnit.Framework;

namespace Forum.Tests.RepositoryTests
{
    public class UsersRepositoryTests
    {
        [Test]
        public async Task UsersRepository_GetAll_ReturnAllValues()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UsersRepository(context);
                var users = await usersRepository.GetAll();
                
                Assert.AreEqual(2 , context.Users.Count());
            }
        }

        [Test]
        public async Task UsersRepository_GetById_ReturnsSingleValue()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UsersRepository(context);
                var user = await usersRepository.GetById(1);
                
                Assert.AreEqual("Yura" , user.Username);
                Assert.AreEqual("Admin" , user.Role);
                Assert.AreEqual("qwert" , user.Password);
            }
        }

        [Test]
        public async Task UsersRepository_CreateUser_AddsToInMemoryDatabase()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UsersRepository(context);
                await usersRepository.Create(new User
                {
                    Role = "User", UserId = 3, Username = "Ksyusha", Password = "xenon",
                    RegistrationDate = new DateTime(2021, 9, 3)
                });
                context.SaveChanges();
                
                Assert.AreEqual(3 , context.Users.Count());
            }
        }

        [Test]
        public async Task UsersRepository_DeleteById_DeleteFromDatabase()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UsersRepository(context);
                usersRepository.Delete(2);
                context.SaveChanges();
                var users = await usersRepository.GetAll();
                
                Assert.AreEqual(1 , context.Users.Count());
            }
        }

        [Test]
        public async Task UsersRepository_UpdateUser_UpdatesUserInDatabase()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UsersRepository(context);
                usersRepository.Update(new User
                {
                    Role = "User", UserId = 2, Username = "Polina", Password = "12345",
                    RegistrationDate = new DateTime(2016, 10, 10)
                });
                context.SaveChanges();
                var updatedUser = await usersRepository.GetById(2);
                Assert.AreEqual("12345" , updatedUser.Password);
            }
        }

        [Test]
        public async Task UsersRepository_FindUser_ReturnsUserByCondition()
        {
            using (var context = new ForumContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var usersRepository = new UsersRepository(context);
                var foundUser = usersRepository.Find(p => p.UserId == 2).FirstOrDefault();
                
                Assert.AreEqual("Polina" , foundUser.Username);
                Assert.AreEqual("User" , foundUser.Role);
            }
        }
    }
}