﻿using System;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Forum.Tests.UnitTestHelpers
{
    static class UnitTestHelper
    {
        public static DbContextOptions<ForumContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<ForumContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new ForumContext(options))
            {
                SeedData(context);
            }
            return options;
        }
        
        public static void SeedData(ForumContext context)
        {
            context.Users.Add(new User
            {
                Role = "Admin", UserId = 1, Username = "Yura", Password = "qwert",
                RegistrationDate = new DateTime(2015, 12, 12)
            });
            context.Users.Add(new User
            {
                Role = "User", UserId = 2, Username = "Polina", Password = "qwert",
                RegistrationDate = new DateTime(2016, 10, 10)
            });
            context.Posts.Add(new Post {PostId = 1 , Text = "First post" , Title = "Post 1" , Published = new DateTime(2020, 7, 21) ,UserId = 1});
            context.Posts.Add(new Post {PostId = 2 , Text = "Second post" , Title = "Post 2" , Published = new DateTime(2019, 7, 21) , UserId = 1});
            context.Posts.Add(new Post {PostId = 3 , Text = "Third post" , Title = "Post 3" , Published = new DateTime(2021, 7, 21) , UserId = 2});
            
            context.Comments.Add(new Comment { PostId = 1, Message = "First comment", CommentId = 1 , CreationDate = new DateTime(2020 , 12,12), UserId = 1});
            context.Comments.Add(new Comment { PostId = 1, Message = "Second comment", CommentId = 2 , CreationDate = new DateTime(2020 , 11,11), UserId = 2});
            context.Comments.Add(new Comment { PostId = 3, Message = "Third comment", CommentId = 3 , CreationDate = new DateTime(2019 , 10,10), UserId = 2});

            context.SaveChanges();
        }
        
        
    }
}