﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BLL.Interfaces;
using Forum.BLL.Services;
using Forum.BLL.DTO;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Moq;
using NUnit.Framework;

namespace Forum.Tests.BusinessTests
{
    public class UserServiceTest
    {
        private Mapper mapper;

        public UserServiceTest()
        {
            mapper = new Mapper(new MapperConfiguration(config =>
            {
                config.CreateMap<User, UserDTO>().ReverseMap();
                config.CreateMap<Post, PostDTO>().ReverseMap();
                config.CreateMap<Comment, CommentDTO>().ReverseMap();
            }));
        }
        
        
        [TestCase(1)]
        [TestCase(3)]
        [TestCase(2)]
        public async Task UserService_GetUserById_ReturnsUser(int userId)
        {
            var users = TestUsers().ToList();
            var expected = users.FirstOrDefault(u => u.UserId == userId);
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(m => m.Users.GetById(userId)).
                ReturnsAsync(TestUsers().
                    FirstOrDefault(user => user.UserId == userId));

            IUserService userService = new UserService(moq.Object);
            var actual = await userService.GetUserById(userId);

            var actualUsers = mapper.Map<User>(actual);

            Assert.AreEqual(expected.Role, actualUsers.Role);
            Assert.AreEqual(expected.Username, actualUsers.Username);
            Assert.AreEqual(expected.RegistrationDate, actualUsers.RegistrationDate);

        }
        
        
        [TestCase(-1)]
        [TestCase(-10)]
        public async Task UserService_GetUserByIdBelowZero_ThrowsArgumentException(int userId)
        {
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(m => m.Users.GetById(userId)).
                ReturnsAsync(TestUsers().
                    FirstOrDefault(user => user.UserId == userId));

            IUserService userService = new UserService(moq.Object);
            
            Assert.ThrowsAsync<ArgumentException>(() => userService.GetUserById(userId));
        }
        

        private IEnumerable<User> TestUsers()
        {
            return new List<User>
            {
                new User
                {
                    Role = "Admin", UserId = 1, Username = "Yura", Password = "admin",
                    RegistrationDate = new DateTime(2015, 12, 12)
                },
                new User
                {
                    Role = "User", UserId = 2, Username = "Jessica", Password = "jessica123",
                    RegistrationDate = new DateTime(2015, 12, 12)
                },
                new User
                {
                    Role = "User", UserId = 3, Username = "Valeria", Password = "pass",
                    RegistrationDate = new DateTime(2015, 12, 12)
                }
            };
        }

        
        [Test]
        public async Task UserService_CreateUser_AddToDatabase()
        {
            // Arrange
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Users.Create(It.IsAny<User>()));                                       // Приймає будь-якого користувача
            IUserService userService = new UserService(moq.Object);
            UserDTO user = new UserDTO
                { Role = "User", Username = "CreatedUser", RegistrationDate = new DateTime(1999, 12, 30), UserId = 10 };
            string pass = "pass";
            
            // Act
            await userService.CreateUser(user , pass);
            
            // Assert
            moq.Verify(x => x.Users.Create(It.Is<User>(u => u.Username == user.Username)), Times.Once);
            //Перевіряєм, чи визвався метод з ЮзерСервіса саме один раз через виклик його з ЮнiтОфВорка
        }

        
        [Test]
        public async Task UserService_UpdateUser_ChangeUserInfoInDB()
        {
            UserDTO updatedUser = new UserDTO
            {
                Role = "Admin", Username = "UpdatedUser", RegistrationDate = new DateTime(1999, 12, 30), UserId = 2
            };
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Users.Update(It.IsAny<User>()));
            moq.Setup(m => m.Users.GetById(updatedUser.UserId)).
                ReturnsAsync(TestUsers().
                    FirstOrDefault(user => user.UserId == updatedUser.UserId));
            IUserService userService = new UserService(moq.Object);

            await userService.UpdateUser(updatedUser);
            var actual = await userService.GetUserById(updatedUser.UserId);
            
            //Assert.AreEqual(updatedUser.Role , actual.Role);
            Assert.AreEqual(updatedUser.Username , actual.Username);
        }

        
        [Test]
        public async Task UserService_UpdateUser_ThrowExceptionIfNull()
        {
            UserDTO updatedUser = null;
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Users.Update(It.IsAny<User>()));
            IUserService userService = new UserService(moq.Object); 

            Assert.ThrowsAsync<ArgumentNullException>(() => userService.UpdateUser(updatedUser));
        }
        
        
        [TestCase(2)]
        [TestCase(1)]
        [TestCase(3)]
        public async Task UserService_DeleteUser_DeletesUserFromDatabase(int userId)
        {
            // Arrange
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Users.Delete(It.IsAny<int>()));
            moq.Setup(m => m.Posts.Find(It.IsAny<Func<Post, bool>>()));

            IUserService userService = new UserService(moq.Object);
            
            // Act
            await userService.Delete(userId);
            
            // Assert
            moq.Verify(u => u.Users.Delete(userId), Times.Once);
        }

        
        [TestCase("Yura","admin")]
        [TestCase("Valeria","pass")]
        public async Task UserService_Login_ReturnsUser(string username , string password)
        {
            // Arrange
            User expected = TestUsers().FirstOrDefault(u => u.Username == username && u.Password == password);
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Users.GetAll()).
                ReturnsAsync(TestUsers());

            // Act
            IUserService userService = new UserService(moq.Object);
            UserDTO actual = await userService.Login(username, password);
            User actualUser = mapper.Map<User>(actual);
            
            // Assert
            Assert.AreEqual(expected.Username, actualUser.Username);
            Assert.AreEqual(expected.RegistrationDate, actualUser.RegistrationDate);
        }

        [Test]
        public async Task UserService_GetAll_ReturnsAllUsers()
        {
            // Arrange
            var expectedUsers = TestUsers().ToList();
            Mock<IUnitOfWork> moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Users.GetAll())
                .ReturnsAsync(TestUsers());
            IUserService userService = new UserService(moq.Object);
            
            // Act
            var actual = await userService.GetAllUsers();
            var actualUsers = mapper.Map<IEnumerable<User>>(actual).ToList();
            
            // Assert
            for (int i = 0; i < actualUsers.Count(); i++)
            {
                Assert.AreEqual(expectedUsers[i].Username,  actualUsers[i].Username);
                Assert.AreEqual(expectedUsers[i].RegistrationDate, actualUsers[i].RegistrationDate);
                Assert.AreEqual(expectedUsers[i].UserId, actualUsers[i].UserId);
            }
        }
    }
}