﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.BLL.Services;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Moq;
using NUnit.Framework;

namespace Forum.Tests.BusinessTests
{
    public class PostServiceTests
    {
        private Mapper mapper;

        public PostServiceTests()
        {
            mapper = new Mapper(new MapperConfiguration(config =>
            {
                config.CreateMap<User, UserDTO>().ReverseMap();
                config.CreateMap<Post, PostDTO>().ReverseMap();
                config.CreateMap<Comment, CommentDTO>().ReverseMap();
            }));
        }

        [Test]
        public async Task PostService_CreatePost_AddPostToDB()
        {
            // Arrange
            PostDTO newPost = new PostDTO
            {
                Published = new DateTime(2021, 9, 8),
                Text = "Test Post",
                Title = "Test",
                UserId = 1,
                PostId = 100
            };
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Posts.Create(It.IsAny<Post>()));
            var postService = new PostService(moq.Object);
            // Act
            await postService.CreatePost(newPost);
            
            // Assert
            moq.Verify(u => u.Posts.Create(It.Is<Post>(x => x.PostId == newPost.PostId)) , Times.Once);
        }
        
        [Test]
        public void PostService_CreatePost_ThrowsExceptionIfNull()
        {
            // Arrange
            PostDTO newPost = null;
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Posts.Create(It.IsAny<Post>()));
            var postService = new PostService(moq.Object);

            // Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => postService.CreatePost(newPost));
        }

        [TestCase(100)]
        [TestCase(101)]
        [TestCase(102)]
        public async Task PostService_GetPost_ReturnsPostDTO(int postId)
        {
            // Arrange
            Post expected = TestPosts().FirstOrDefault(p => p.PostId == postId);
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Posts.GetById(postId))
                .ReturnsAsync(TestPosts().FirstOrDefault(p => p.PostId == postId));
            var postService = new PostService(moq.Object);
            
            // Act
            PostDTO postDto = await postService.GetPost(postId);
            Post actual = mapper.Map<Post>(postDto);
            
            // Assert
            Assert.AreEqual(expected.Text , actual.Text);
            Assert.AreEqual(expected.Title , actual.Title);
            Assert.AreEqual(expected.PostId , actual.PostId);
        }

        [TestCase(-5)]
        public void PostService_GetPost_ThrowExceptionIfNull(int postId)
        {
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Posts.GetById(postId))
                .ReturnsAsync(TestPosts().FirstOrDefault(p => p.PostId == postId));
            var postService = new PostService(moq.Object);

            Assert.ThrowsAsync<ArgumentException>(() => postService.GetPost(postId));
        }

        private IEnumerable<Post> TestPosts()
        {
            return new List<Post>
            {
                new Post
                {
                    Published = new DateTime(2021, 9, 8),
                    Text = "Test Post",
                    Title = "Test",
                    UserId = 1,
                    PostId = 100
                },
                new Post
                {
                    Published = new DateTime(2020, 9, 8),
                    Text = "Test Post 2",
                    Title = "Test2",
                    UserId = 2,
                    PostId = 101
                },
                new Post
                {
                    Published = new DateTime(2020, 9, 8),
                    Text = "Test Post 2",
                    Title = "Test2",
                    UserId = 3,
                    PostId = 102
                }
            };
        }

        private IEnumerable<Comment> TestComments()
        {
            return new List<Comment>
            {
                new Comment
                {
                    PostId = 100, Message = "First comment", CommentId = 1, CreationDate = new DateTime(2020, 12, 12),
                    UserId = 1
                },
                new Comment
                {
                    PostId = 101, Message = "Second comment", CommentId = 2, CreationDate = new DateTime(2020, 11, 11),
                    UserId = 2
                },
                new Comment
                {
                    PostId = 102, Message = "Third comment", CommentId = 3, CreationDate = new DateTime(2019, 10, 10),
                    UserId = 2
                }
            };
        }

        [Test]
        public async Task PostService_EditPost_EditsAPostInDB()
        {
            PostDTO postDto = new PostDTO
            {
                Published = new DateTime(1999, 12, 30),
                Text = "Updated post",
                Title = "Updated post title",
                PostId = 100,
                UserId = 1
            };
            Mock<IUnitOfWork> moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Posts.GetById(postDto.PostId))
                .ReturnsAsync(TestPosts().FirstOrDefault(post => post.PostId == postDto.PostId));
            moq.Setup(u => u.Posts.Update(It.IsAny<Post>()));
            IPostService postService = new PostService(moq.Object);

            await postService.EditPost(postDto);
            var actual = await postService.GetPost(postDto.PostId);
            
            Assert.AreEqual(postDto.Text , actual.Text);
            Assert.AreEqual(postDto.Title , actual.Title);
        }
        
        [Test]
        public async Task PostService_EditPost_ThrowsExIfNull()
        {
            PostDTO postDto = null;
            Mock<IUnitOfWork> moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Posts.Update(It.IsAny<Post>()));
            IPostService postService = new PostService(moq.Object);

            Assert.ThrowsAsync<ArgumentNullException>(() => postService.EditPost(postDto));
        }

        [Test]
        public async Task PostService_GetPosts_ReturnsAllPost()
        {
            List<Post> expectedPosts = TestPosts().ToList();
            Mock<IUnitOfWork> moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Posts.GetAll()).ReturnsAsync(TestPosts());
            IPostService postService = new PostService(moq.Object);

            var actualPostsDtos = await postService.GetPosts();
            var actualPosts = mapper.Map<IEnumerable<Post>>(actualPostsDtos).ToList();

            for (int i = 0; i < expectedPosts.Count; i++)
            {
                Assert.AreEqual(expectedPosts[i].Text , actualPosts[i].Text);
                Assert.AreEqual(expectedPosts[i].PostId , actualPosts[i].PostId);
                Assert.AreEqual(expectedPosts[i].Title , actualPosts[i].Title);
            }
        }

        [TestCase(1,3)]
        [TestCase(1,1)]
        public void PostService_GetPostsAtPage_ReturnsPostsAtPage(int page , int postAtPage)
        {
            var expected = TestPosts().OrderByDescending(p => p.Published).
                Skip((page-1) * postAtPage)
                .Take(postAtPage)
                .ToList();
            Mock<IUnitOfWork> moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Posts.GetAll())
                .ReturnsAsync(TestPosts());
            IPostService postService = new PostService(moq.Object);

            var actualPostDtos = postService.GetPostsAtPage(page, postAtPage);
            var actual = mapper.Map<IEnumerable<Post>>(actualPostDtos).ToList();
            
            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i].PostId , actual[i].PostId);
                Assert.AreEqual(expected[i].Title , actual[i].Title);
                Assert.AreEqual(expected.Count , actual.Count);
            }
        }

        [Test]
        public async Task PostService_AddComment_AddsCommentToDB()
        {
            CommentDTO newComment = new CommentDTO
            {
                Message = "New comment",
                CommentId = 1,
                CreationDate = new DateTime(2017, 12, 12),
                PostId = 100,
                UserId = 1
            };
            Mock<IUnitOfWork> moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Comments.Create(It.IsAny<Comment>()));
            moq.Setup(u => u.Posts.GetById(newComment.PostId))
                .ReturnsAsync(TestPosts().FirstOrDefault(post => post.PostId == newComment.PostId));
            IPostService postService = new PostService(moq.Object);

            await postService.AddComment(newComment);
            
            moq.Verify(u => u.Comments.Create(It.Is<Comment>(x => x.Message == newComment.Message)), Times.Once());
        }

        [TestCase(1)]
        [TestCase(2)]
        public async Task PostService_DeleteComment_DeletesCommentFromDB(int commentId)
        {
            var moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Comments.Delete(commentId));
            IPostService postService = new PostService(moq.Object);

            await postService.DeleteComment(commentId);
            
            moq.Verify(u => u.Comments.Delete(It.IsAny<int>()) , Times.Once);
        }

        [TestCase(-1)]
        public async Task PostService_DeleteComment_ThrowArgumentExceptionIfIdIsInvalid(int commentId)
        {
            var moq = new Mock<IUnitOfWork>();
            IPostService postService = new PostService(moq.Object);

            Assert.ThrowsAsync<ArgumentException>(() => postService.DeleteComment(commentId));
        }
        
        [TestCase(100)]
        public async Task PostService_GetPostComments_ReturnsPostComments(int postId)
        {
            var expected = TestComments().Where(c => c.PostId == postId).ToList();
            Mock<IUnitOfWork> moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Comments.GetAll()).ReturnsAsync(TestComments());
            IPostService postService = new PostService(moq.Object);

            var commentsDtos = await postService.GetPostComments(postId);
            List<Comment> actual = mapper.Map<IEnumerable<Comment>>(commentsDtos).ToList();

            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i].CommentId , actual[i].CommentId);
                Assert.AreEqual(expected[i].Message , actual[i].Message);
            }
        }

        [Test]
        public void PostService_PostCount_GetAmountOfPosts()
        {
            int expected = TestPosts().Count();
            Mock<IUnitOfWork> moq = new Mock<IUnitOfWork>();
            moq.Setup(u => u.Posts.Count())
                .Returns(TestPosts().Count);
            IPostService postService = new PostService(moq.Object);
            Assert.AreEqual(expected , postService.PostsCount());
        }
    }
}