﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.DAL.Entities
{
    [Table("Comments")]
    public class Comment
    {
        [Key , DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }

        [Required]
        public int PostId { get; set; }
        
        [Required]
        public Post Post { get; set; }

        [Required]
        public int UserId { get; set; }
        
        [Required]
        public User User { get; set; }

        [Required, MaxLength(1500)]
        public string Message { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }
    }
}