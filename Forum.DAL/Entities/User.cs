﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.DAL.Entities
{ 
    [Table("Users")]
    public class User
    {
        [Key , DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [Required, MaxLength(50)]
        [RegularExpression(@"^[a-zA-Z0-9]{1,50}$", 
            ErrorMessage = "Characters are not allowed.")]
        public string Username { get; set; }

        [Required , MaxLength(20)]
        public string Password { get; set; }

        [Required]
        public DateTime RegistrationDate { get; set; }
        
        public ICollection<Post> Posts { get; set; }

        public ICollection<Comment> Comments { get; set; }
        
        [Required]
        public string Role { get; set; }
    }
}