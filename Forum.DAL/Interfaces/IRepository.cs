﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        Task Create(T item);
        void Delete(int id);
        void Update(T item);
        IEnumerable<T> Find(Func<T, bool> predicate);
        int Count();
    }
}