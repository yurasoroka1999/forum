﻿using System;
using System.Threading.Tasks;
using Forum.DAL.Entities;

namespace Forum.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Post> Posts { get; }
        IRepository<Comment> Comments { get; }
        IRepository<User> Users { get; }
        Task<bool> Save();
    }
}