﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Repositories
{
    public class PostsRepository : IRepository<Post>
    {
        private ForumContext _context;

        public PostsRepository(ForumContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Post>> GetAll()
        {
            return await _context.Posts.ToListAsync();
        }

        public async Task<Post> GetById(int id)
        {
            return await _context.Posts.FindAsync(id);
        }

        public async Task Create(Post item)
        {
            await _context.Posts.AddAsync(item);
        }

        public void Delete(int id)
        {
            _context.Posts.Remove(_context.Posts.Find(id));
        }

        public void Update(Post item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.Posts.Update(item);
        }

        public IEnumerable<Post> Find(Func<Post, bool> predicate)
        {
            return _context.Posts.Where(predicate);
        }
        
        public int Count()
        {
            return _context.Posts.Count();
        }
    }
}