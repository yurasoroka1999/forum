﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Repositories
{
    public class CommentsRepository : IRepository<Comment>
    {
        private ForumContext _context;              
        
        public CommentsRepository(ForumContext context)
        {
            _context = context;
        }
        
        
        public async Task<IEnumerable<Comment>> GetAll()
        {
            return await _context.Comments.ToListAsync();
        }

        public async Task<Comment> GetById(int id)
        {
            return await _context.Comments.FindAsync(id);
        }

        public async Task Create(Comment item)
        {
            await _context.Comments.AddAsync(item);
        }

        public void Delete(int id)
        {
            _context.Comments.Remove(_context.Comments.Find(id));
        }

        public void Update(Comment item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.Comments.Update(item);
        }

        public IEnumerable<Comment> Find(Func<Comment, bool> predicate)
        {
            return  _context.Comments.Where(predicate);
        }

        public int Count()
        {
            return _context.Comments.Count();
        }
    }
}