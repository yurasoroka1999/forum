﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Repositories
{
    public class UsersRepository : IRepository<User>
    {
        private ForumContext _context;
        
        public UsersRepository(ForumContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User> GetById(int id)
        {
            return  await _context.Users.FindAsync(id);
        }

        public async Task Create(User item)
        {
            await _context.Users.AddAsync(item);
        }

        public void Delete(int id)
        {
            _context.Users.Remove(_context.Users.Find(id));
        }

        public void Update(User item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.Users.Update(item);
        }

        public IEnumerable<User> Find(Func<User, bool> predicate)
        {
            return _context.Users.Where(predicate);
        }
        
        public int Count()
        {
            return _context.Users.Count();
        }
    }
}