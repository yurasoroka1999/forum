﻿using System;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;

namespace Forum.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private ForumContext _context;
        private PostsRepository posts;
        private CommentsRepository comments;
        private UsersRepository users;
        private bool disposed = false;
        public UnitOfWork()
        {
            _context = new ForumContext();
        }
        
        //props
        public IRepository<Post> Posts
        {
            get { return posts ?? (posts = new PostsRepository(_context)); }
            //if (posts == null)
            //{
            //    posts = new PostsRepository(_context);
            //}
            //return posts;
        }

        public IRepository<Comment> Comments
        {
            get{ return comments ?? (comments = new CommentsRepository(_context)); }
        }

        public IRepository<User> Users
        {
            get { return users ?? (users = new UsersRepository(_context)); }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                disposed = true;
            }
        }
        
        public async Task<bool> Save()
        {
            return await _context.SaveChangesAsync() != 0;              //якщо 0 - нуль значень записано в  базу данних,вона не змінилась
        }
    }
}