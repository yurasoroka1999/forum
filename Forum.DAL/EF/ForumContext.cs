﻿using System;
using System.Configuration;
using System.IO;
using Forum.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Forum.DAL.EF
{
    public class ForumContext : DbContext
    {
        //public ForumContext()
        //{
        //    Database.EnsureCreated();                                                                                     //Create if doesn't exist , but database update doesn;t work
        //}
        public ForumContext() { }
        public ForumContext(DbContextOptions<ForumContext> options):base(options) { }                                       // for InMemory Unit testing 
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //IConfigurationRoot configuration = new ConfigurationBuilder()
            //    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            //    .AddJsonFile("appsettings.json")
            //    .Build();
            //
            //string connection = configuration.GetConnectionString("MyConnection");
            
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseSqlServer(
                        @"Server=(localdb)\MSSqlLocaldb;Database=ForumDB;Trusted_Connection=True;MultipleActiveResultSets=true");
            }

        }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<User> Users { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //
            //modelBuilder.Entity<User>()
            //    .HasMany(u => u.Posts)
            //    .WithOne(p => p.User)
            //    .HasForeignKey(u => u.UserId)
            //    .OnDelete(DeleteBehavior.Cascade);
            //
            ////1
            //modelBuilder.Entity<User>()
            //    .HasMany(u => u.Comments)
            //    .WithOne(c => c.User)
            //    .HasForeignKey(u => u.UserId)
            //    .OnDelete(DeleteBehavior.Cascade);
            //
            ////2
            //modelBuilder.Entity<Post>()
            //    .HasMany(p => p.Comments)
            //    .WithOne(c => c.Post)
            //    .HasForeignKey(u => u.PostId)
            //    .OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<User>().HasData(
                new User
                {
                    UserId = 1,
                    Username = "admin",
                    Password = "admin",
                    RegistrationDate = DateTime.Now,
                    Role = "Admin"
                },
                new User
                {
                    UserId = 2,
                    Username = "user",
                    Password = "user",
                    RegistrationDate = DateTime.Now,
                    Role = "User"
                }
            );

            // добавить початкові коментарі і пости потім !
            // або винести в окремий клас Seed (extension)
        }
    }
}