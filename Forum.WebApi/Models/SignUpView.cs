﻿using System.ComponentModel.DataAnnotations;

namespace Forum.WebApi.Models
{
    public class SignUpView
    {
        [Required(ErrorMessage = "Username field is required. ")]
        [MaxLength(50 , ErrorMessage = "Username is too long. Max 50 symbols. ")]
        [RegularExpression(@"^[a-zA-Z0-9]{1,50}$", 
            ErrorMessage = "Characters are not allowed. ")]
        public string Username { get; set; }
        
        [Required(ErrorMessage = "Password field is required. ")]
        [MaxLength(20 , ErrorMessage = "Username is too long.Max 20 symbols. ")]
        [RegularExpression(@"^[a-zA-Z0-9]{4,}$", 
            ErrorMessage = "Invalid password. \nMin 4 symbols;\nNo whitespaces;")]
        public string Password { get; set; }
        
        [Required(ErrorMessage = "Password field is required. ")]
        [MaxLength(20 , ErrorMessage = "Username is too long. Max 20 symbols. ")]
        [RegularExpression(@"^[a-zA-Z0-9]{4,}$", 
            ErrorMessage = "Invalid password. \nMin 4 symbols;\nNo whitespaces;")]
        [Compare("Password", ErrorMessage = "Password doesn't match. ")]
        public string RetryPassword { get; set; }
    }
}