﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Forum.WebApi.Helpers
{
    public class JWTOptions
    {
        public const string Issuer = "PersonalForum";
        public const string Audience = "ForumUser";
        private const string Key = "a329be76146f44ef9e608acae3225957";
        public const int Lifetime = 1;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}