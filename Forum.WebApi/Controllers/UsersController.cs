using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.WebApi.Helpers;
using Forum.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Forum.WebApi.Controllers
{
    [AllowAnonymous]    
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        
        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody]SignUpView signUpView)
        {
            //ModelState
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (signUpView.Password == signUpView.RetryPassword)                                                        //Якщо паролі співпадають, то пускаємо далі
            {
                if (await _userService.CreateUser(new UserDTO { Username = signUpView.Username }, signUpView.Password))
                    return Ok();
            }
            
            return BadRequest();
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody]LoginView loginView)
        {
            //modelState is valid
            var registeredUser = await _userService.Login(loginView.UserName, loginView.Password);
            if (registeredUser == null)
                return Unauthorized();

            var jwt = new JwtSecurityToken(
                issuer: JWTOptions.Issuer,
                audience: JWTOptions.Audience,
                notBefore: DateTime.UtcNow,
                claims: new []
                {
                    new Claim(JwtRegisteredClaimNames.Sub , registeredUser.Username),
                    new Claim(ClaimsIdentity.DefaultNameClaimType, registeredUser.UserId.ToString()),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType , registeredUser.Role)
                },
                expires: DateTime.UtcNow.Add(TimeSpan.FromDays(JWTOptions.Lifetime)),
                signingCredentials: new SigningCredentials(JWTOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var token = new JwtSecurityTokenHandler().WriteToken(jwt);

            return Ok(new
            {
                registeredUser.Username,
                registeredUser.Role,
                token
            });
        }
        
        
        // DELETE api/users/1
        [Authorize(Roles = "Admin")]
        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            if (await _userService.GetUserById(userId) == null)
                return NotFound();
            return Ok(await _userService.Delete(userId));
        }


        // GET api/users/all
        
        [HttpGet("all")]
        public async Task<IActionResult> GetAllUsers()
        {
            return Ok(await _userService.GetAllUsers());
        }
    }
}