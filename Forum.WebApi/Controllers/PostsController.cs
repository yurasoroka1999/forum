using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.BLL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Forum.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private IPostService _postService;
        public PostsController(IPostService postService)
        {
            _postService = postService;
        }
        
        [Authorize(Roles = "Admin")]
        [Route("Test")]
        public string Test()
        {
            return "You are authorized";
        }

        [Authorize(Roles = "User")]
        [Route("TestUser")]
        public string TestUser()
        {
            return "You are authorized as a user";
        }

        //    P O S T S    //
        
        // GET api/posts/all
        [HttpGet("all")]
        public async Task<IActionResult> GetPosts()
        {
            var posts = await _postService.GetPosts();
            return Ok(posts);
        }
        
        // GET api/posts/show/?page=1&size=5
        [HttpGet("Show/")]
        public IActionResult ShowPosts([FromQuery]int page , [FromQuery]int size)
        {
            try
            {
                var posts = _postService.GetPostsAtPage(page , size);
                return Ok(posts);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }
        

        // POST api/posts/create
        [Authorize]
        [HttpPost("Create")]
        public async Task<IActionResult> CreatePost([FromBody]PostDTO postDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int currentUserId = int.Parse(User.Identity.Name);

            postDto.UserId = currentUserId;
            await _postService.CreatePost(postDto);

            return StatusCode(201);
        }

        
        // GET api/posts/{postId}
        [HttpGet("{postId}")]
        public async Task<IActionResult> GetPost(int postId)
        {
            PostDTO postDto = await _postService.GetPost(postId);
            if (postDto != null)
                return Ok(postDto);
            
            return NotFound();
        }


        // DELETE api/posts/{postId}
        [Authorize(Roles = "Admin")]
        [HttpDelete("{postId}")]
        public async Task<IActionResult> DeletePost(int postId)
        {
            if (await _postService.DeletePost(postId))
            {
                return Ok();
            }
            return NotFound();
        }
        
        
        // PUT api/posts/edit
        [Authorize]
        [HttpPut("{postId}/edit")]
        public async Task<IActionResult> EditPost(int postId , PostDTO postDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int currentUserId = int.Parse(User.Identity.Name);
            postDto.UserId = currentUserId;
            postDto.PostId = postId;
            await _postService.EditPost(postDto);
            return Ok();
        }
        
        // GET api/posts/count
        [HttpGet("count")]
        public int PostsCount()
        {
            return _postService.PostsCount();
        }


        //  C O M M E N T S  //

        // GET api/posts/1/comments
        [HttpGet("{postId}/comments")]
        public async Task<IActionResult> GetPostComments(int postId)
        {
            var comments = await _postService.GetPostComments(postId);
            return Ok(comments);
        }


        // POST api/posts/1/comments
        [Authorize]
        [HttpPost("{postId}/comments")]
        public async Task<IActionResult> AddComment(int postId , CommentDTO commentDto)
        {
            int currentUserId = int.Parse(User.Identity.Name);
            commentDto.UserId = currentUserId;
            commentDto.PostId = postId;

            if (await _postService.AddComment(commentDto))
                return Created($"api/posts/{postId}/comments" , commentDto);

            return BadRequest();
        }

        // DELETE api/posts/comments/1
        [Authorize(Roles = "Admin")]
        [HttpDelete("comments/{commentId}")]
        public async Task<IActionResult> DeleteComment(int commentId)
        {
            if (await _postService.DeleteComment(commentId))
                return Ok();

            return NotFound();
        }

        // GET api/posts/1/comments
        [HttpGet("{postId}/commentscount")]
        public async Task<int> PostsCommentsCount(int postId)
        {
            var comments = await _postService.GetPostComments(postId);
            return comments.Count();
        }
    }
}