﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.BLL.DTO
{
    public class CommentDTO
    {
        public int CommentId { get; set; }

        public int PostId { get; set; }
        public int UserId { get; set; }

        [Required(ErrorMessage = "Comment can not be empty")]
        [MaxLength(1500, ErrorMessage = "Too long comment. Max 1500 symbols")]
        public string Message { get; set; }

        public DateTime CreationDate { get; set; }
    }
}