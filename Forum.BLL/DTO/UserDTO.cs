﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.BLL.DTO
{
    public class UserDTO
    {
        public int UserId { get; set; }
        
        [Required(ErrorMessage = "Username is required"), MaxLength(50 , ErrorMessage = "Username is too long. Max length 50 symbols")]
        [RegularExpression(@"^[a-zA-Z0-9]{1,50}$", 
            ErrorMessage = "Characters are not allowed.")]
        public string Username { get; set; }
        
        public DateTime RegistrationDate { get; set; }

        public string Role { get; set; }        

    }
}