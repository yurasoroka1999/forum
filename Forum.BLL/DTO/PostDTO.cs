﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.BLL.DTO
{
    public class PostDTO
    {
        public int PostId { get; set; }

        public int UserId { get; set; }
        
        
        [Required(ErrorMessage = "Title can not be empty"), MaxLength(500 , ErrorMessage = "Too long title. Max 500 symbols")]
        public string Title { get; set; }

        
        [Required(ErrorMessage = "Text cannot be empty")]
        public string Text { get; set; }

        
        public DateTime Published { get; set; }
    }
}