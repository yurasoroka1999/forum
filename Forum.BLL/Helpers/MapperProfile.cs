﻿using AutoMapper;
using Forum.BLL.DTO;
using Forum.DAL.Entities;

namespace Forum.BLL.Helpers
{
    public static class MapperProfile
    {
        public static MapperConfiguration ConfigureMapper()
        {
            MapperConfiguration config = new MapperConfiguration(config =>
            {
                config.CreateMap<User, UserDTO>().ReverseMap();
                config.CreateMap<Post, PostDTO>().ReverseMap();
                config.CreateMap<Comment, CommentDTO>().ReverseMap();
            });
            return config;
        }
    }
}