﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Forum.BLL.DTO;

namespace Forum.BLL.Interfaces
{
    public interface IPostService
    {
        Task<bool> CreatePost(PostDTO postDTO);
        Task<PostDTO> GetPost(int postId);
        Task<bool> EditPost(PostDTO postDTO);
        Task<bool> DeletePost(int postId);
        IEnumerable<PostDTO> GetPostsAtPage(int page , int postsOnPage);
        Task<IEnumerable<PostDTO>> GetPosts();
        Task<bool> AddComment(CommentDTO commentDTO);
        Task<bool> DeleteComment(int commentId);
        Task<IEnumerable<CommentDTO>> GetPostComments(int postId);
        //void SaveChanges();
        int PostsCount();
    }
}