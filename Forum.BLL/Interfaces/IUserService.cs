﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Forum.BLL.DTO;

namespace Forum.BLL.Interfaces
{
    public interface IUserService
    {
        Task<UserDTO> GetUserById(int id);
        Task<bool> CreateUser(UserDTO user, string password);
        Task<bool> UpdateUser(UserDTO user);
        Task<bool> Delete(int id);
        Task<UserDTO> Login(string username, string password);
        Task<IEnumerable<UserDTO>> GetAllUsers();
        //Task<UserDTO> FindByUsername(string name);
        //void SaveChanges();
    }
}