﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Helpers;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;

namespace Forum.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        
        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = new Mapper(MapperProfile.ConfigureMapper());
        }
        
        
        // Methods
        
        public async Task<UserDTO> GetUserById(int id)
        {
            if (id < 1)
                throw new ArgumentException($"UserId {id} is not valid");

            var user =  await _unitOfWork.Users.GetById(id);

            if (user == null)
                throw new ArgumentNullException($"User with {id} ID not found");
            
            var userDto = _mapper.Map<UserDTO>(user);
            return userDto;
        }

        public async Task<bool> CreateUser(UserDTO userDto, string password)
        {
            //if (!IsValidUsername(userDto.Username))
            //    throw new ArgumentException($"Required field {userDto.Username}");
            if (IsUsernameRegistered(userDto.Username))
                throw new ArgumentException("Username is already registered");
            //if (!IsPasswordValid(password))
            //    throw new ArgumentException($"{password} is not valid");

            userDto.RegistrationDate = DateTime.Now;
            userDto.Role = Roles.User;

            User newUser = _mapper.Map<User>(userDto);
            newUser.Password = password;
            await _unitOfWork.Users.Create(newUser);

            return await _unitOfWork.Save();
        }

        public async Task<bool> UpdateUser(UserDTO user)
        {
            if (user == null)
            {
                throw new ArgumentNullException($"{user} is null");
            }
            User userToUpdate = await _unitOfWork.Users.GetById(user.UserId);
            userToUpdate.Username = user.Username;
            //
            //userToUpdate.Role = user.Role;

            return await _unitOfWork.Save();
        }

        public async Task<bool> Delete(int id)
        {
            var posts = _unitOfWork.Posts.Find(post => post.UserId == id);
            foreach (var userPost in posts)
            {
                await ClearPostComments(userPost.PostId);           //удаляєм коменти з поста користувача, якого також удаляєм
                _unitOfWork.Posts.Delete(userPost.PostId);
            }
            
            _unitOfWork.Users.Delete(id);
            return await _unitOfWork.Save();
        }

        public async Task<UserDTO> Login(string username, string password)
        {

            var allUsers = await _unitOfWork.Users.GetAll();
            User user = allUsers
                .FirstOrDefault(user => user.Username == username && user.Password == password);
            
            //User user = _unitOfWork.Users.GetAll().GetAwaiter().GetResult()
            //    .FirstOrDefault(user => user.Username == username && user.Password == password);
            
            var userDto = _mapper.Map<UserDTO>(user);
            return userDto;
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsers()
        {
            var allUsers = await _unitOfWork.Users.GetAll();
            return _mapper.Map<IEnumerable<UserDTO>>(allUsers);
        }

        //public async Task<UserDTO> FindByUsername(string name)
        //{
        //    var userList = await _unitOfWork.Users.GetAll();
        //    var user = userList.FirstOrDefault(user => user.Username == name);
        //    UserDTO userDto = _mapper.Map<UserDTO>(user);
        //    return userDto;
        //}


        //helper methods
        private bool IsValidUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
                return false;
            return Regex.IsMatch(username, @"^[a-zA-Z0-9]{3,50}$");
        }

        private bool IsUsernameRegistered(string username)
        {
            return _unitOfWork.Users.GetAll().GetAwaiter().GetResult().Any(user => user.Username == username);
            //true , if is already registered
            //false - if empty;
        }

        private bool IsPasswordValid(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                return false;

            return true;
            //can use regex expression
        }

        private async Task ClearPostComments(int postId)
        {
            var comments = await _unitOfWork.Comments.GetAll();
            comments = comments.Where(c => c.PostId == postId);
            if (comments.Any())
            {
                foreach (var comment in comments)
                {
                    _unitOfWork.Comments.Delete(comment.CommentId);
                }
            }
        }
    }
}