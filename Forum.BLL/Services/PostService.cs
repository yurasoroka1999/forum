﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Helpers;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;

namespace Forum.BLL.Services
{
    public class PostService : IPostService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        //private const int PostsOnPage = 1;
        
        public PostService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = new Mapper(MapperProfile.ConfigureMapper());
        }
        
        
        //Methods
        
        public async Task<bool> CreatePost(PostDTO postDTO)
        {
            if (postDTO == null)
            {
                throw new ArgumentNullException($"{postDTO} is null");
            }
            var post = _mapper.Map<Post>(postDTO);
            post.Published = DateTime.Now;
            
            await _unitOfWork.Posts.Create(post);
            return await _unitOfWork.Save();
        }

        public async Task<PostDTO> GetPost(int postId)
        {
            if (postId < 0)
            {
                throw new ArgumentException($"{postId} ID is not valid");
            }
            Post post = await _unitOfWork.Posts.GetById(postId);
            return _mapper.Map<PostDTO>(post);
        }

        public async Task<bool> EditPost(PostDTO postDTO)
        {
            if (postDTO == null)
            {
                throw new ArgumentNullException($"{postDTO} is null");
            }
            Post postToEdit = await _unitOfWork.Posts.GetById(postDTO.PostId); 
            //можна добавити можливість редагувати тільки заголовок або тільки текст
            postToEdit.Title = postDTO.Title;
            postToEdit.Text = postDTO.Text;
            postToEdit.Published = DateTime.Now;
            return await _unitOfWork.Save();
        }

        public async Task<bool> DeletePost(int postId)
        {
            if (postId < 0)
            {
                throw new ArgumentException($"{postId} ID is invalid");
            }
            IEnumerable<Comment> postComments = await PostComments(postId);
            if (postComments.Any())
            {
                foreach (var comment in postComments)
                {
                    _unitOfWork.Comments.Delete(comment.CommentId);
                }
            }
            _unitOfWork.Posts.Delete(postId);
            return await _unitOfWork.Save();
        }

        public async Task<IEnumerable<PostDTO>> GetPosts()
        {
            IEnumerable<Post> posts = await _unitOfWork.Posts.GetAll();
            posts = posts.OrderByDescending(post => post.Published);     //newer posts on the top
            return _mapper.Map<IEnumerable<PostDTO>>(posts);
        }


        public IEnumerable<PostDTO> GetPostsAtPage(int page , int postsOnPage)
        {
            if (page < 1 || postsOnPage < 1)
            {
                throw new ArgumentException($"{postsOnPage} or {page} has invalid value");
            }
            IEnumerable<Post> posts = _unitOfWork.Posts.GetAll().GetAwaiter().GetResult().
                OrderByDescending(p => p.Published).
                Skip(--page * postsOnPage).
                Take(postsOnPage).
                ToList();
        
            return _mapper.Map<IEnumerable<PostDTO>>(posts);
        }

        public async Task<bool> AddComment(CommentDTO commentDTO)
        {
            if (commentDTO == null)
            {
                throw new ArgumentNullException($"{commentDTO} is null");
            }
            if (_unitOfWork.Posts.GetById(commentDTO.PostId) == null)
            {
                throw new ArgumentNullException($"Post with this {commentDTO.PostId} ID doesn't exist");
            }
            //await GetPost(commentDTO.PostId);
            commentDTO.CreationDate = DateTime.Now;

            var comment = _mapper.Map<Comment>(commentDTO);
            await _unitOfWork.Comments.Create(comment);
            return await _unitOfWork.Save();
        }

        public async Task<bool> DeleteComment(int commentId)
        {
            if (commentId < 0)
            {
                throw new ArgumentException($"{commentId} ID is not valid");
            }
            _unitOfWork.Comments.Delete(commentId);
            return await _unitOfWork.Save();
        }

        public async Task<IEnumerable<CommentDTO>> GetPostComments(int postId)
        {
            if (postId < 0)
            {
                throw new ArgumentNullException($"Invalid {postId} post ID");
            }
            IEnumerable<Comment> allComments = await _unitOfWork.Comments.GetAll();

            var postComments = allComments.Where(c => c.PostId == postId).
                OrderByDescending(c => c.CreationDate).
                ToList();

            return _mapper.Map<IEnumerable<CommentDTO>>(postComments);
        }

        public int PostsCount()
        {
            return _unitOfWork.Posts.Count();
        }
        
        private async Task<IEnumerable<Comment>> PostComments(int postId)
        {
            IEnumerable<Comment> allComments = await _unitOfWork.Comments.GetAll();
            return allComments.Where(c => c.PostId == postId);
        }
    }
}