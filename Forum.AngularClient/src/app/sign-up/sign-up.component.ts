import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ISignUp } from '../models/sign-up.model';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  signUpModel = <ISignUp>{};
  form = new FormGroup({
    Username: new FormControl(),
    Password: new FormControl(),
    RetryPassword: new FormControl()
  });

  constructor(public service:SharedService , private router:Router) { }

  ngOnInit(): void {
    if(localStorage.getItem("token")){
      this.router.navigateByUrl("/posts");
    }
  }

  Submit(){
    this.service.signUp(this.signUpModel).subscribe(data => {
      console.log(data);
      this.router.navigateByUrl('/posts');
    },
    err => {
      const validationErrors = err.error["errors"];
      Object.keys(validationErrors).forEach(prop => {
        const formControl = this.form.get(prop);
        if (formControl) {
          formControl.setErrors({
            serverError: validationErrors[prop]
          });
        }
      })
      // console.log(validationErrors);
    });
  }
}
