import { ComponentFactoryResolver, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { IPost } from './models/post.model';
import { IComment } from './models/comment.model';
import { IUser } from './models/user.model';
import { ISignIn } from './models/sign-in.mode';
import { ISignUp } from './models/sign-up.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  
  readonly APIurl = "https://localhost:44362/api/";

  getHeaders():HttpHeaders{
    return new HttpHeaders({
      "Content-type":"application/json",
      "Authorization":`Bearer ${(localStorage.getItem('token')!)}`
    })
  }

  constructor(private http: HttpClient) {
  }

  // getPosts(){
  //   return this.http.get<IPost[]>(this.APIurl + 'posts/all');
  // }

  getPost(postId:string){
    return this.http.get<IPost>(this.APIurl + 'posts/' + postId);
  }

  getComments(postId:string){
    return this.http.get<IComment[]>(this.APIurl + 'posts/' + postId + '/comments');
  }

  getUsers(){
    return this.http.get<IUser[]>(this.APIurl + 'users/all');
  }

  signIn(loginUser:ISignIn){
    return this.http.post(this.APIurl + 'users/login' , loginUser);
  }

  signUp(registerUser:ISignUp){
    return this.http.post(this.APIurl + 'users/register' , registerUser);
  }

  createPost(post:IPost){
    //console.log(`Bearer ${(localStorage.getItem('token')!)}`)
    return this.http.post(this.APIurl + 'posts/create' , post , {
      headers: new HttpHeaders({
        "Content-type":"application/json",
        "Authorization":`Bearer ${(localStorage.getItem('token')!)}`
      })
    });
  }


  createComment(postId:string , comment:IComment){
    return this.http.post(this.APIurl + 'posts/' + postId + '/comments' , comment , {
      headers: new HttpHeaders({
        "Content-type":"application/json",
        "Authorization":`Bearer ${(localStorage.getItem('token')!)}`
      })
    });
  }


  deletePost(postId:string){
    return this.http.delete(this.APIurl + 'posts/' + postId , {
      headers: this.getHeaders()} );
  }

  deleteComment(commentId:string){
    return this.http.delete(this.APIurl + "posts/comments/" + commentId , {
      headers: this.getHeaders()
    });
  }

  getRole():string{
    let result = "";
    if(localStorage.getItem("token")!= null){
      let token = localStorage.getItem("token");
      token = JSON.parse(atob(token!.split(".")[1]))!;
      result += token!["http://schemas.microsoft.com/ws/2008/06/identity/claims/role" as any];
    }
    return result;
  }

  getCurrentId():string{
    let result = "";
    if(localStorage.getItem("token")!= null){
      let token = localStorage.getItem("token");
      token = JSON.parse(atob(token!.split(".")[1]))!;
      result += token!["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" as any];
    }
    return result;
  }

  getCurrentUsername():string{
    let result = "";
    if(localStorage.getItem("token")!= null){
      let token = localStorage.getItem("token");
      token = JSON.parse(atob(token!.split(".")[1]))!;
      result += token!["sub" as any];
    }
    return result;
  }

  getPostsCount(){
    return this.http.get<number>(this.APIurl + "posts/count");
  }

  getPosts(page:number , size:number){
    let params = new HttpParams();
    params = params.append('page' , String(page));
    params = params.append('size' , String(size));

    return this.http.get<IPost[]>(this.APIurl + 'posts/show/' , {params})
  }

  editPost(postId:string , editedPost:IPost){
    return this.http.put(this.APIurl + "posts/" + postId + "/edit" , editedPost , {headers: this.getHeaders()});
  }

  deleteUser(userId:number){
    return this.http.delete(this.APIurl + "users/" + userId , { headers: this.getHeaders() });
  }
}