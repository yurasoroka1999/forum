import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';

import { PostsComponent } from './posts/posts.component'; 
import { PostDetailComponent } from './post-detail/post-detail.component';
import { CommentComponent } from './comment/comment.component';
import { AuthGuard } from './auth/auth.guard';
import { NewPostComponent } from './new-post/new-post.component';
import { NewCommentComponent } from './new-comment/new-comment.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UsersComponent } from './users/users.component';
import { HasRoleGuard } from './auth/has-role.guard';


const routes: Routes = [
  { path: '',   redirectTo: '/posts', pathMatch: 'full' },      // homepage
  { path: 'login' , component: SignInComponent },
  { path: 'register' , component: SignUpComponent }, 
  { path: 'posts' , component: PostsComponent},
  { path: 'users' , component: UsersComponent , canActivate:[HasRoleGuard] , data:{role: 'Admin'} },
  { path: 'posts/create' , component: NewPostComponent , canActivate:[AuthGuard]},
  { path: 'posts/:postId' , component: PostDetailComponent ,
  children: [
    { path: 'comments' , component: CommentComponent},
    { path: 'addcomment' , component:NewCommentComponent , canActivate:[AuthGuard] },
  ]},
  { path: 'notfound' , component:PageNotFoundComponent},
  { path: '**' , redirectTo: 'notfound'}     // wildcard route , handle when users attempt to navigate to a part of your application that does not exist. 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
