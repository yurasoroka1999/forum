import { Component , OnInit} from '@angular/core';
import { SharedService } from '../shared.service';
import { IPost } from '../models/post.model';
import { PageEvent } from '@angular/material/paginator';
import { IUser } from '../models/user.model';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit{

  posts!: IPost[];
  postsCount!:number;         //all rows in post table
  users = <IUser[]>{};

  loadingSpinner = true;
  pageSize:number = 5;        //start posts on the page


  constructor(public postService: SharedService){
  }


  ngOnInit(): void {
    this.initPostsOnPage();

    this.postService.getPostsCount().subscribe(data => {
      console.log(data);                                                     //As it is an asynchronous call, you can't know when it will be returned. So move the postsSlice into the subscribe function
      this.postsCount = data;                                                //remove spinner with *ngIf
    });
  }


  initPostsOnPage(){
    this.postService.getPosts(1, this.pageSize).subscribe(data => {
      this.posts = data;
      console.log(this.posts);
      this.loadingSpinner = false; 
    })
  }

  //lazy pagination
  onPaginateChange(event : PageEvent){
    this.loadingSpinner = true; 
    let page = event.pageIndex;
    let size = event.pageSize;
    page = page + 1;
    this.postService.getPosts(page , size).subscribe(data => {
      console.log(data);
      this.posts = data;
      this.loadingSpinner = false; 
    });
  }


  // changePage(event: PageEvent){
  //   const startIndex = event.pageIndex * event.pageSize;
  //   let endIndex = startIndex + event.pageSize;
  //   if (endIndex > this.posts.length){
  //     endIndex = this.posts.length;
  //   }
  //   this.postsSlice = this.posts.slice(startIndex , endIndex);
  // }

  
  //nextPage(){
  //  ++PostsComponent.currentPage;
  //  if(PostsComponent.currentPage > 1){
  //    this.previousPageDisabled = false;
  //  }
  //
  //  this.postService.getPosts(PostsComponent.currentPage).subscribe(data => this.posts = data);
  //  this.postService.getPosts(PostsComponent.currentPage + 1).subscribe(data => this.possibleFuturePosts = data);
  //
  //  if(this.possibleFuturePosts?.length == 0){
  //    PostsComponent.nextPageDisabled = true;
  //  }
  //  this.router.navigate(['/posts/page/' , PostsComponent.currentPage]);
  //}
  //
  //previousPage(){
  //  --PostsComponent.currentPage;
  //  if(PostsComponent.currentPage == 1){
  //    this.previousPageDisabled = true;
  //  }
  //  this.postService.getPosts(PostsComponent.currentPage).subscribe(data => this.posts = data);
  //  if(this.posts?.length != 0){
  //    PostsComponent.nextPageDisabled = false;
  //  }
  //  this.router.navigate(['/posts/page/' , PostsComponent.currentPage]);
  //}
  //
  //
  ////getter for static field
  //get staticCurrentPage(){
  //  return PostsComponent.currentPage;
  //}
  //
  //get staticNextPageDisabled(){
  //  return PostsComponent.nextPageDisabled;
  //}

}
