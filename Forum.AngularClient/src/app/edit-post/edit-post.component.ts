import { Component, OnInit , Inject } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { IPost } from '../models/post.model';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {

  editedPost = <IPost>{};      //empty instance of IPost intarface
  editLoading:boolean = false;
  form = new FormGroup({
    Text: new FormControl(),
    Title: new FormControl()
  })

  constructor( 
    @Inject(MAT_DIALOG_DATA) private data:IPost ,            //inject MAT_DIALOG_DATA data params , which i passed into a mat dialog data params
    private dialogRef: MatDialogRef<EditPostComponent>,
    private service:SharedService , private router:Router) { }

  ngOnInit(): void {
  }


  
  submit(){  
    this.editLoading = true;
    //console.log(this.data.postId);
    this.service.editPost(this.data.postId.toString() ,this.editedPost).subscribe(data => {
      //console.log(data);
      this.editLoading = false;
      //console.log("edited");
      this.dialogRef.close(true);
      //console.log("closed");
    },
    err => {
      const validationErrors = err.error["errors"];
      Object.keys(validationErrors).forEach(prop => {
        const formControl = this.form.get(prop);
        if (formControl) {
          formControl.setErrors({
            serverError: validationErrors[prop]
          });
        }
      })
    });
  }
}
