import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from '../shared.service';
import { IComment } from '../models/comment.model';
import { IUser } from '../models/user.model';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  comments!:IComment[];
  postId!:string;
  users!:IUser[];
  role!:string;
  progressBarLoading:boolean = false;

  commentsEnd:number = 1;   //початкова кількість коментарів, які виводяться користувачу
  commentsStep:number = 1;  //скільки показувати по кнопці SHOW MORE
  buttonDisabled:boolean = true;

  constructor(public postService: SharedService , private route:ActivatedRoute , private router:Router) { 
  }

  ngOnInit(): void {
  
    this.postId = this.route.parent?.snapshot.params["postId"];
    this.postService.getComments(this.postId).subscribe(data => {
      this.comments = data;
      if(this.comments.length != 0 ){
        this.buttonDisabled = false;
      }
    });
    this.postService.getUsers().subscribe(data => this.users = data);
    this.role = this.postService.getRole();
  }

  getUsernameById(id: number):string{
    return this.users.find(user => user.userId === id)!.username.toString();
  }

  showMoreComments(){
    if((this.commentsEnd + this.commentsStep) >= this.comments.length){
      this.buttonDisabled = true;
    }
    this.commentsEnd += this.commentsStep;
  }

  deleteComment(commentId:number): void{
    this.progressBarLoading = true;
    this.postService.deleteComment(commentId.toString()).subscribe(data => {
      console.log(data); 
      this.ngOnInit();
      this.progressBarLoading = false;
      
    });
  }

}
