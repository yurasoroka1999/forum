import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ISignIn } from '../models/sign-in.mode';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  signInModel: ISignIn = {
    username:"",
    password:""
  };
  signInError!:string;

  constructor(public service:SharedService , private router:Router) { }

  ngOnInit(): void {
    if(localStorage.getItem("token")){
      this.router.navigateByUrl("/posts");
    }
  }

  Submit(){
    this.service.signIn(this.signInModel).subscribe((data:any) => {
      console.log(data);
      localStorage.setItem('token', data.token);        //property from web.api method
      this.router.navigateByUrl('/posts');
    },
    err=>{
      if(err.status == 401){
        console.log("incorrect username");
        this.signInError = "Incorrect username or password";
      }
    })
  }
}
