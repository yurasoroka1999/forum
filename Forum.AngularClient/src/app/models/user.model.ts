export interface IUser{
    userId:number;
    username:string;
    registrationDate:string;
    //
    role:string;
}