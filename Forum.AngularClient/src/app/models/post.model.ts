export interface IPost {
    postId: number;
    userId: number;
    title: string;
    text: string;
    published: string;
  }