export interface IComment {
    commentId: number;
    postId: number;
    userId: number;
    message: string;
    creationDate: string;
  }