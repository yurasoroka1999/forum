import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup , ReactiveFormsModule} from '@angular/forms';
import { Router } from '@angular/router';
import { IPost } from '../models/post.model';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {

  newPost = <IPost>{};      //empty instance of IPost intarface
  form = new FormGroup({
    Title: new FormControl(),
    Text: new FormControl()
  });

  constructor(public service:SharedService , private router:Router) { }

  ngOnInit(): void {
  }

  Submit(){
    this.service.createPost(this.newPost).subscribe(data => {
      console.log(data);
      this.router.navigateByUrl('/posts');
    },
    err =>{
      //console.log(err.error["errors"].Title.toString());
      const validationErrors = err.error["errors"];
      Object.keys(validationErrors).forEach(prop => {
        const formControl = this.form.get(prop);
        if (formControl) {
          formControl.setErrors({
            serverError: validationErrors[prop]
          });
        }
      })
      //console.log(this.form.get('Title')?.errors?.serverError)
    });
  }
}
