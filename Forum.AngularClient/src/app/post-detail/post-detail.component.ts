import { HttpErrorResponse } from '@angular/common/http';
import { Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { MatDialog} from '@angular/material/dialog';
import { ActivatedRoute , Router } from '@angular/router';
import { EditPostComponent } from '../edit-post/edit-post.component';
import { IPost } from '../models/post.model';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {

  post!:IPost;
  postId!:string;
  role!:string;
  loadingSpinner:boolean = false;
  progressBarLoading:boolean = false;

  constructor(public postService:SharedService , private route:ActivatedRoute , private router:Router, private dialog: MatDialog) { }

 
  ngOnInit(): void {
    this.loadingSpinner = true;
    this.postId = this.route.snapshot.params['postId'];
    //console.log(this.id)
    this.postService.getPost(this.postId).subscribe(data => {
      this.post = data;
      this.loadingSpinner = false;
    },
    err => {
      if(err instanceof HttpErrorResponse){
        this.router.navigateByUrl('/notfound');
      }
    });
    this.role = this.postService.getRole();
    console.log(this.role);
  }

  deletePost(): void{
    this.progressBarLoading = true;
    this.postService.deletePost(this.postId).subscribe(data => {
      console.log(data); 
      this.progressBarLoading = false;
      this.router.navigateByUrl('/posts');
    });
  }


  openEditDialog(){
    let dialogRef = this.dialog.open(EditPostComponent , 
      //passing a current post object into a dialogConfigParam as "Data"
      {
        data:{
          postId: this.post.postId,
          published: this.post.published,
          text: this.post.text,
          title: this.post.title,
          userId: this.post.userId
        }
      });

    dialogRef.afterClosed().subscribe(result => {this.ngOnInit(); console.log("closed from mother")});   
  }

  isYourPost(currentPostId:number){
    return this.postService.getCurrentId() === currentPostId.toString();
  }
}
