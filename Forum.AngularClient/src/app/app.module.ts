import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SignInComponent } from './sign-in/sign-in.component';
import { MatIconModule } from '@angular/material/icon';
import { CommentComponent } from './comment/comment.component';
import { MatCardModule } from '@angular/material/card';
import { PostsComponent } from './posts/posts.component';
import { MatDividerModule } from '@angular/material/divider';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion'

import { SharedService } from './shared.service';
import { HttpClientModule } from '@angular/common/http';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { MatBadgeModule } from '@angular/material/badge';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { NewPostComponent } from './new-post/new-post.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { NewCommentComponent } from './new-comment/new-comment.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import { EditPostComponent } from './edit-post/edit-post.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UsersComponent } from './users/users.component';
import {MatTableModule} from '@angular/material/table';
 
@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    CommentComponent,
    PostsComponent,
    PostDetailComponent,
    NewPostComponent,
    NewCommentComponent,
    EditPostComponent,
    SignUpComponent,
    PageNotFoundComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatBadgeModule,
    MatPaginatorModule,
    MatInputModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
