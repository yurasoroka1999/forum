import { Component, OnInit } from '@angular/core';
import { IUser } from '../models/user.model';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users!:IUser[];
  displayedColumns: string[] = ['userId', 'username', 'role' , 'action' ];
  loading:boolean = false;

  constructor(private service:SharedService) { }

  ngOnInit(): void {
    this.service.getUsers().subscribe(data => this.users = data);
  }

  deleteUser(userId:number){
    this.loading = true
    this.service.deleteUser(userId).subscribe(data => {
      this.loading = false;
      console.log(data);
      this.ngOnInit();
    });
  }

}
