import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from './shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  constructor(private router:Router , public service:SharedService) { }

  navBarSwitcher(){
    if(localStorage.getItem("token") == null){
      return true;
    }
    else{
      return false;
    }
  }

  logOut(){
    if(localStorage.getItem("token") != null){
      localStorage.removeItem("token");
      this.router.navigateByUrl("/posts");
    }
  }
}
