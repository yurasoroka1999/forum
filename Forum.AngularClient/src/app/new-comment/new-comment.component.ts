import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IComment } from '../models/comment.model';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-new-comment',
  templateUrl: './new-comment.component.html',
  styleUrls: ['./new-comment.component.css']
})
export class NewCommentComponent implements OnInit {

  newComment = <IComment>{};      //empty instance of IComment intarface
  postId!:string;
  form = new FormGroup({
    Message: new FormControl()
  });

  constructor(public service:SharedService , private route:ActivatedRoute , private router:Router) { }

  ngOnInit(): void {
  }

  Submit(){
    this.postId = this.route.parent?.snapshot.params["postId"];         //parent route
    //console.log(this.postId);
    this.service.createComment(this.postId , this.newComment).subscribe(data => {
      console.log(data);
      this.router.navigateByUrl('/posts/'+this.postId+'/comments');
    },
    err => {
      const validationErrors = err.error["errors"];
      Object.keys(validationErrors).forEach(prop => {
        const formControl = this.form.get(prop);
        if (formControl) {
          formControl.setErrors({
            serverError: validationErrors[prop]
          });
        }
      })
    });
  }

}
